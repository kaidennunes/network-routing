#!/usr/bin/python3


from CS312Graph import *
import time


class NetworkRoutingSolver:
	def __init__( self):
		pass

	def initializeNetwork( self, network ):
		assert( type(network) == CS312Graph )
		self.network = network

	def getShortestPath( self, destIndex ):
		self.dest = destIndex

		path_edges = []
		total_length = 0
		
		current_node = self.network.nodes[self.dest]
		current_node_value = self.node_values[self.dest]
		
		total_length = current_node_value[0]

		# Go find the path that we solved for. Time complexity is O(V + E), where V is the number of nodes in the graph and E the number of edges in the graph. Space complexity is constant.
		while True:
			next_node_index = current_node_value[1]

			# If there is no node to continue onto, then there was no path
			if next_node_index == None:
				import math

				total_length = math.inf
				break
			next_node = self.network.nodes[next_node_index]

			edge_length = 0

			# Throughout the course of the while loop, we can only go through at most E edges, where E is the number of edges in the graph.
			for edge in next_node.neighbors:
				if edge.dest.node_id == current_node.node_id:
					edge_length = edge.length
					break

			path_edges.append( (current_node.loc, next_node.loc, '{:.0f}'.format(edge_length)) )
			if current_node_value[1] == self.source:
				break
			current_node_value = self.node_values[current_node_value[1]]
			current_node = next_node

		return {'cost':total_length, 'path':path_edges}

	# The time complexity of this for the heap implementation is O(VlogV), base 2. For the array implementation, the time complexity is O(V^2). For both implementations, the space complexity is O(V). V is the number of nodes in the graph.
	def computeShortestPaths( self, srcIndex, use_heap=False ):
		self.source = srcIndex
		self.node_values = []

		t1 = time.time()

		# Initialize the priority queue to be whatever it should be. Constant time and space complexity
		priority_queue = None
		if use_heap:
			priority_queue = HeapPriorityQueue()
		else:
			priority_queue = ArrayPriorityQueue()

		# Make every node have an initial distance and previous node of None, except the starting node, which has an initial distance of 0. Also initialize everything in the queue.
		# This is O(V) time and space complexity, where V is the number of nodes.
		for node in self.network.nodes:
			if node.node_id == srcIndex:
				self.node_values.append([0, None])
			else:
				self.node_values.append([None, None])

		# Fill the priority queue with the node IDs. For the heap, the time complexity is O(VlogV) where V is the number of nodes in the graph and log is base 2. For the array, the time complexity is O(V).
		# The space complexity for both implementations is O(V)
		priority_queue.make_queue(self.network.nodes[:], self.source)

		# While we still have a node to process, keep going. This will run at most V times, where V is the number of nodes in the graph.
		while not priority_queue.is_empty():
			# Pull out the smallest node of the priority queue. For the array implementation, this is O(V) time complexity, for the heap, it is O(logV), where V is the number of elements in the heap. Space complexity is constant.
			min_element = priority_queue.delete_min(self.node_values)
			# If the min node is a one without any distance value, that means that the source node can reach no farther places
			if self.node_values[min_element][0] == None:
				break

			# For all the edges that this node has, update the distance values of the nodes. Over the course of the whole while loop, this will run at most E times, where E is the number of edges in the graph.
			# Since we are guaranteed a sparse graph, we can convert that E to be just V.
			for edge in self.network.nodes[min_element].neighbors:
				# If a reachable node has no distance value yet, or a value greater than the current path will give it, then give it the distance value of the current node plus the value of the edge to that reachable node.
				# The first two statements have constant time and space complexity
				if self.node_values[edge.dest.node_id][0] == None or self.node_values[edge.dest.node_id][0] > edge.length + self.node_values[edge.src.node_id][0]:
					self.node_values[edge.dest.node_id][0] = edge.length + self.node_values[edge.src.node_id][0]
					self.node_values[edge.dest.node_id][1] = min_element
					# For the array implementation, this is constant time complexity. For the heap implementation, the time complexity is O(logV), where V is the number of nodes in the heap. The space complexity for both implementations is constant.
					priority_queue.decrease_key(edge.dest.node_id, self.node_values[edge.dest.node_id][0])

		t2 = time.time()

		return (t2-t1)

class ArrayPriorityQueue:
	# Constant time complexity and space complexity
	def __init__( self):
		self.queue = []

	# Constant time complexity and space complexity
	def is_empty(self):
		return (len(self.queue) == 0)

	# Constant time and space complexity
	def insert(self, node):
		self.queue.append(node)

	# Constant time complexity and space complexity
	def decrease_key(self, node, value):	
		pass
	
	# Time complexity of O(V), where V is the number of elements in the queue.
	def delete_min(self, node_values):
		min_node_index = 0
		# This loop runs V times, where V is the number of nodes in the queue.
		for index, node in enumerate(self.queue):
			# See if the node in the queue has a distance value. This is constant time and space complexity
			if node_values[node][0] == None:
				pass
			elif node_values[self.queue[min_node_index]][0] == None:
				min_node_index = index
			elif node_values[self.queue[min_node_index]][0] > node_values[node][0] :
				min_node_index = index

		# Remove the element from the list. We will assume this to be worst case of O(V), where V is the number of nodes in the array, since if the element to pop is the first element, we have to reshift all the following elements
		return self.queue.pop(min_node_index)
	
	# Space complexity is O(V), where V is the number of nodes. Time complexity is also O(V)
	def make_queue(self, nodes, starting_node):
		for node in nodes:
			self.queue.append(node.node_id)

class HeapPriorityQueue:
	# Constant time complexity and space complexity
	def __init__(self):
		self.node_name_value_pairs = []
		self.node_indexes = []
	
	# Constant time complexity
	def is_empty(self):
		return (len(self.node_name_value_pairs) == 0)

	# The time complexity of O(logV), where V is the number of points in the heap. The space complexity is constant.
	def insert(self, node, value):
		# Construct the node pair. Constant time and space complexity.
		node_pair = [node, value]

		# Add the node to the heap. Constant time and space complexity. 
		insert_index = len(self.node_name_value_pairs)
		self.node_name_value_pairs.append(node_pair)
		self.node_indexes.append(insert_index)

		# Bubble the node up the heap to its correct location. This is O(logV) operations, as noted in the method
		self._bubble_up(node_pair)

	# The time complexity of O(logV), where V is the number of points in the heap. The space complexity is constant.
	def decrease_key(self, node, value):
		# Get the index for that node name value pair. Constant time and space complexity.
		node_index = self.node_indexes[node]

		# Update the value for that node. Constant time and space complexity.
		self.node_name_value_pairs[node_index][1] = value
		
		# Bubble the node up the heap to its correct location. This is O(logV) operations, as noted in the method
		self._bubble_up(self.node_name_value_pairs[node_index])
	
	# The time complexity of O(logV), where V is the number of points in the heap. The space complexity is constant.
	def delete_min(self, element_values):
		# Get the min element from the tree
		min_node = self.node_name_value_pairs[0]

		# If it is the last element, then just return it and pop it from the tree.
		if len(self.node_name_value_pairs) == 1:
			self.node_name_value_pairs.pop()
			return min_node[0]

		# Get the node at the end of the heap. Constant time and space complexity.
		node_pair = self.node_name_value_pairs[-1]

		# Stick that node as the new root of the heap. Constant time and space complexity.
		self.node_name_value_pairs[0] = node_pair 

		# Mark the new index of that node. Constant time and space complexity.
		self.node_indexes[node_pair[0]] = 0

		# Mark the old root's index as nothing, just in case. Constant time and space complexity.
		self.node_indexes[min_node[0]] = None

		# Pop off the extra node slot. Constant time complexity.
		self.node_name_value_pairs.pop()

		# Bubble down that node we moved to the root. This is O(logV) operations, as noted in the method
		self._bubble_down(node_pair)

		# Return the min element of the queue
		return min_node[0]
	
	# The time complexity of this is O(VlogV), where V is the number of nodes to be put in the queue. The space complexity is O(V).
	def make_queue(self, nodes, starting_node):
		# The for loop executes V times, where V is the number of nodes
		for index, node in enumerate(nodes):
			# As noted in the following method, insert operations have a time complexity of O(logV), where V is the number of nodes in the heap.
			if starting_node == index:
				self.insert(node.node_id, 0)
			else:
				self.insert(node.node_id, None)
	
	# We only go up from the bottom of the tree up to the root of the tree, one layer each time iteration, for a total of log(V) times in the worst case. So, the time complexity is O(logV), where V is the number of nodes in the heap 
	def _bubble_up(self, node_pair):
		# Get the parent node. Constant time and space complexity.
		parent_node_pair = self._get_parent(node_pair[0])
		
		# If the node has a value equal to or larger than its parent, or has no parent, then stop bubbling up. Constant time and space complexity.
		if parent_node_pair == None or node_pair[1] == None:
			return

		if parent_node_pair[1] == None:
			pass
		elif node_pair[1] >= parent_node_pair[1]:
			return

		# Otherwise, switch the two. Switch the index array to match the new placement. Constant time and space complexity.
		parent_node_index = parent_node_pair[0]
		node_index = node_pair[0]
		temp_index = self.node_indexes[parent_node_index]

		self.node_indexes[parent_node_index] = self.node_indexes[node_index]
		self.node_indexes[node_index] = temp_index

		# Switch the actual name value pairs. Constant time and space complexity.
		temp_node_pair = parent_node_pair
		self.node_name_value_pairs[self.node_indexes[node_pair[0]]] = node_pair
		self.node_name_value_pairs[self.node_indexes[temp_node_pair[0]]] = temp_node_pair


		# Otherwise, continue to bubble up
		self._bubble_up(node_pair)

	# We only go down from the root to the bottom of the tree, going down one layer each time, for a total of log(V) times in the worst case. So, the time complexity is O(logV), where V is the number of nodes in the heap 
	def _bubble_down(self, node_pair):
		# Get the children nodes. Constant time and space complexity.
		children_node_pairs = self._get_children(node_pair[0])

		# If it has no children, then stop bubbling. Constant time and space complexity.
		if children_node_pairs[0] == None and children_node_pairs[1] == None:
			return

		# Otherwise, find which of the children nodes has a smaller value. Constant time and space complexity.
		smaller_child_node_pair = None
		if children_node_pairs[0] == None and children_node_pairs[1] != None:
			smaller_child_node_pair = children_node_pairs[1]
		if children_node_pairs[0] != None and children_node_pairs[1] == None:
			smaller_child_node_pair = children_node_pairs[0]
		elif children_node_pairs[0][1] == None:
			smaller_child_node_pair = children_node_pairs[1]
		elif children_node_pairs[1][1] == None:
			smaller_child_node_pair = children_node_pairs[0]
		elif children_node_pairs[0][1] > children_node_pairs[1][1]:
			smaller_child_node_pair = children_node_pairs[1]
		else:
			smaller_child_node_pair = children_node_pairs[0]

		# If that node has a equal or larger value than the current node, then stop bubbling down. Constant time and space complexity.
		if node_pair[1] == None and smaller_child_node_pair != None:
			pass
		elif smaller_child_node_pair[1] == None or smaller_child_node_pair[1] >= node_pair[1]:
			return

		# Otherwise, switch them. Constant time and space complexity.
		child_node_index = smaller_child_node_pair[0]
		node_index = node_pair[0]
		temp_value_index = self.node_indexes[child_node_index]

		self.node_indexes[child_node_index] = self.node_indexes[node_index]
		self.node_indexes[node_index] = temp_value_index

		# Switch the actual name value pairs. Constant time and space complexity.
		temp_node_pair = smaller_child_node_pair
		self.node_name_value_pairs[temp_value_index] = node_pair
		self.node_name_value_pairs[self.node_indexes[temp_node_pair[0]]] = temp_node_pair

		# Continue to bubble down
		self._bubble_down(node_pair)

	# Gets the parent node of the given node, if there is any. Constant time and space complexity.
	def _get_parent(self, node):
		node_index = self.node_indexes[node]
		# If we are at the root, there is no parent
		if node_index == 0:
			return None

		parent_index = (node_index - 1) // 2
		return (self.node_name_value_pairs[parent_index])
	
	# Gets the children node of the given node, if any. Constant time and space complexity
	def _get_children(self, node):
		node_index = self.node_indexes[node]
		first_child_index = (node_index * 2) + 1
		second_child_index = (node_index * 2) + 2

		first_child = None
		second_child = None

		number_nodes = len(self.node_name_value_pairs)
		if number_nodes > first_child_index:
			first_child = self.node_name_value_pairs[first_child_index]
		if number_nodes > second_child_index:
			second_child = self.node_name_value_pairs[second_child_index]
		return [first_child, second_child]